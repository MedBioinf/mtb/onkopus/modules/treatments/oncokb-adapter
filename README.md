# OncoKB-Adapter

Adapter for querying the OncoKB API to retrieve clinical significance data on the effect of treatments on genetic alterations. 

### Run locally

Run the Onkopus OncoKB-Adapter by starting the Flask application. The adapter requires a cache directory path and a path to a cache log file.
The cache directory may be defined by setting an environment variable CACHE_DIR. Additionally, the adapter requires an OncoKB API token to access the API. Define the OncoKB token with the environment variable ONCOKB_KEY.
The variables may be defined in the terminal before starting the adapter:
```
export CACHE_DIR=/mnt/oncokb/
export LOG_FILE=/mnt/oncokb/cache.log
export ONCOKB_KEY=
python3 app.py
```

### Build and run Docker container

### Data Processing 

The OncoKB-Adapter extracts a set of features from the OncoKB API and stores it in the normalized feature section ("oncokb_feature_norm"). 
The adapter performs the following steps to normalize features: 

- Mapping of feature keys
- Mapping of feature values
- Mapping of evidence levels to Onkopus evidence level score system

#### Mapping of feature keys

#### Mapping of feature values

