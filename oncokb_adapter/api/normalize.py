import copy
import traceback
import oncokb_adapter.conf.read_config as conf_reader
import oncokb_adapter.tools.genome_positions

agg_keys = ['gene', 'variant', 'disease', 'drugs', 'evidence_level', 'evidence_type', 'citation_url','citation_id', 'source','response','evidence_statement']
oncokb_extract_keys_agg = ['hugoSymbol','alteration','disease','drugs','level','','citation_id','citation_id','source','highestSensitiveLevel','approvedIndications']
oncokb_extract_keys_1 = ['hugoSymbol', 'alteration']
oncokb_extract_keys = []

# evidence level mapping
oncokb_levels = [ "LEVEL_1", "LEVEL_2", "LEVEL_3A", "LEVEL_3B", "LEVEL_4", "LEVEL_R1", "LEVEL_R2" ]
onkopus_levels = [ "1", "2", "2", "3", "4", "4", "4" ]


def map_evidence_levels(variant_data):
    """

    :param variant_data:
    :return:
    """
    for var in variant_data.keys():
        if 'oncokb_features_norm' in variant_data[var]:
            for match_type in conf_reader.match_types:
                for result in variant_data[var]['oncokb_features_norm'][match_type]:
                    if result["evidence_level"] in oncokb_levels:
                        index = oncokb_levels.index(result["evidence_level"])
                        result["evidence_level_onkopus"] = onkopus_levels[index]

    return variant_data


def split_multiple_pmids_in_separate_evidence_results(variant_data):
    """

    :param variant_data:
    :return:
    """
    section_name = "oncokb_features_norm"
    for var in variant_data.keys():
        print("sections ",var,": ",list(variant_data[var].keys()))
        treatments_new = []
        if section_name in variant_data[var].keys():
            for result in variant_data[var][section_name]:
                pmid_urls = result['citation_id']#.split(",")

                for pmid_url in pmid_urls:
                    result_new = {}
                    for feature in result:
                        result_new[feature] = copy.deepcopy(result[feature])
                        # result_new["citation_url"] = pmid_url
                        result_new["citation_id"] = pmid_url
                        result_new["citation_url"] = conf_reader.config["SOURCE_SPECIFIC"]["PMID_BASE_URL"] + pmid_url

                    treatments_new.append(result_new)
        variant_data[var][section_name] = treatments_new
    return variant_data

def normalize_evidence_level(evidence_level):
    level, restype = evidence_level.split("_")
    if restype == "R1":
        return "Resistant"
    elif restype == "R2":
        return "Resistant"
    elif restype == "1":
        return "1"
    elif restype == "2":
        return "3"
    elif restype == "2A":
        return "1"
    elif restype == "2B":
        return "3"
    elif restype == "3A":
        return "2"
    elif restype == "3B":
        return "3"
    else:
        return ""

def extract_response_type(evidence_level):
    """
    Extracts the response type from the OncoKB evidence level assignment

    :param evidence_level:
    :return:
    """
    level,restype = evidence_level.split("_")
    if restype == "R1":
        return "Resistant"
    elif restype == "R2":
        return "Resistant"
    elif restype == "1":
        return "Sensitive"
    elif restype == "2":
        return "Sensitive"
    elif restype == "3A":
        return "Sensitive"
    elif restype == "3B":
        return "Sensitive"
    else:
        return ""


def normalize_oncokb_feature_keys(variant_data,genome_locations,genes,aa_exchanges):
    """
    Selects a set of features that are present in all evidence databases from the OncoKB API features and assigns the
    same names to all feature keys

    :param variant_data:
    :return:
    """
    variant_data_new = copy.deepcopy(variant_data)
    for var in variant_data.keys():
            variant_data_new[var]['oncokb_features_norm'] = {}

            for match_type in conf_reader.match_types:
                oncokb_extracted_data = []
                if match_type not in variant_data_new[var]['oncokb_features_norm']:
                    variant_data_new[var]['oncokb_features_norm'][match_type] = []

                if 'oncokb' in variant_data[var]:
                    if match_type in variant_data[var]['oncokb']:
                        if 'treatments' in variant_data[var]['oncokb'][match_type]:
                            for i, treatment in enumerate(variant_data[var]['oncokb'][match_type]['treatments']):
                                #if len(oncokb_extracted_data) <= i:
                                #    oncokb_extracted_data.append({})

                                for association in variant_data[var]['oncokb'][match_type]['treatments'][i]['alterations']:

                                    try:
                                        if (genes is not None) and (aa_exchanges is not None):
                                            aa_exchange = aa_exchanges[genome_locations.index(var)]
                                            rec_match_type = oncokb_adapter.tools.genome_positions.recognize_match_type(association,
                                                None, aa_exchange)
                                        else:
                                            rec_match_type = "same_position_any_mutation"
                                    except:
                                        print(traceback.format_exc())
                                        rec_match_type = "same_position"

                                    for pmid in variant_data[var]['oncokb'][match_type]['treatments'][i]["pmids"]:
                                        dc = {}

                                        # biomarker
                                        dc['biomarker'] = variant_data[var]['oncokb'][match_type]['query']['hugoSymbol'] + \
                                                          " " + \
                                                          association

                                        # disease
                                        dc['disease'] = variant_data[var]['oncokb'][match_type]['treatments'][i]['levelAssociatedCancerType']['mainType']['name']
                                        dc['disease_id'] = \
                                            variant_data[var]['oncokb'][match_type]['treatments'][i]['levelAssociatedCancerType']['mainType']['id']

                                        # drugs
                                        drugs = []
                                        for drug in variant_data[var]['oncokb'][match_type]['treatments'][i]['drugs']:
                                            add_drug = {}
                                            add_drug['drug_name'] = drug['drugName']
                                            add_drug['ncit_code'] = drug['ncitCode']
                                            drugs.append(add_drug)
                                        dc['drugs'] = drugs

                                        # evidence level
                                        dc['evidence_level'] = normalize_evidence_level(variant_data[var]['oncokb'][match_type]['treatments'][i]['level'])
                                        dc['evidence_level_onkopus'] = normalize_evidence_level(
                                            variant_data[var]['oncokb'][match_type]['treatments'][i]['level'])
                                        dc['fda_level'] = variant_data[var]['oncokb'][match_type]['treatments'][i]['fdaLevel']

                                        # evidence type
                                        dc['evidence_type'] = ""

                                        # citation ID and URL
                                        dc['citation_id'] = pmid
                                        dc['citation_url'] = conf_reader.config["SOURCE_SPECIFIC"]["PMID_BASE_URL"] + pmid

                                        # source
                                        dc['source'] = "oncokb"
                                        dc["match_type"] = rec_match_type

                                        # response type
                                        dc['response'] = extract_response_type(variant_data[var]['oncokb'][match_type]['treatments'][i]
                                                                                                     ['level'])

                                        # evidence statement
                                        dc['evidence_statement'] = variant_data[var]['oncokb'][match_type]['treatments'][i]['description']

                                        #oncokb_extracted_data.append(copy.deepcopy(dc))
                                        if rec_match_type not in variant_data_new[var]['oncokb_features_norm']:
                                            variant_data_new[var]['oncokb_features_norm'][rec_match_type] = []
                                        variant_data_new[var]['oncokb_features_norm'][rec_match_type].append(copy.deepcopy(dc))

                #variant_data_new[var]['oncokb_features_norm'][match_type] = oncokb_extracted_data

    return variant_data_new


def extract_oncokb_data(variant_data):
    """
    Extracts a defined set of features from the OncoKB API response in key-value format

    :param variant_data:
    :return:
    """
    for var in variant_data.keys():
        if conf_reader.config['MODULES']['ONCOKB_PREFIX'] in variant_data[var]:
            treatment_data = variant_data[var][conf_reader.config['MODULES']['ONCOKB_PREFIX']]['treatments']

            # extract OncoKB data (Lv1)
            for key in oncokb_extract_keys:
                oncokb_extracted_data[key] = \
                    variant_data[var][conf_reader.config['MODULES']['ONCOKB_PREFIX']][key]

            # Extract treatment data (Lv2)
            for i, result in enumerate(treatment_data):
                oncokb_extracted_data = {}

                # Extract features from query section
                for key in oncokb_extract_keys_1:
                    if key in variant_data[var][conf_reader.config['MODULES']['ONCOKB_PREFIX']]['query']:
                        oncokb_extracted_data[key] = \
                        variant_data[var][conf_reader.config['MODULES']['ONCOKB_PREFIX']]['query'][key]

                if 'drugs' not in oncokb_extracted_data:
                    oncokb_extracted_data['drugs'] = ''
                for d in result['drugs']:
                    oncokb_extracted_data['drugs'] += d['drugName'] + ','
                oncokb_extracted_data['drugs'] = oncokb_extracted_data['drugs'].rstrip(',')

                oncokb_extracted_data['level'] = result['level']
                oncokb_extracted_data['disease'] = result['levelAssociatedCancerType']["mainType"]['name']
                oncokb_extracted_data['approvedIndications'] = ",".join(result['approvedIndications'])

                if 'citation_id' not in oncokb_extracted_data:
                    oncokb_extracted_data['citation_id'] = ''
                for pmid in result['pmids']:
                    oncokb_extracted_data['citation_id'] += pmid + ","
                oncokb_extracted_data['citation_id'] = oncokb_extracted_data['citation_id'].rstrip(',')

                oncokb_extracted_data['source'] = 'oncokb'

                if 'oncokb_features' not in variant_data[var]:
                    variant_data[var]['oncokb_features'] = []
                variant_data[var]['oncokb_features'].append(copy.deepcopy(oncokb_extracted_data))

                variant_data = normalize_oncokb_feature_keys(variant_data)
                variant_data = split_multiple_pmids_in_separate_evidence_results(variant_data)

    return variant_data
