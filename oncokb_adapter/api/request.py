import traceback
from oncokb_adapter.conf import read_config as conf_reader
import requests, copy
from oncokb_adapter.tools.genome_positions import parse_genome_position, get_hg19_positions_as_keys
from oncokb_adapter.api.normalize import normalize_oncokb_feature_keys, extract_oncokb_data, map_evidence_levels,split_multiple_pmids_in_separate_evidence_results


def process_request(cache=None, genome_locations=None, genome_locations_hg19=None,genome_version_q=None,
                    genes=None,
                    aa_exchanges=None
                    ):
    """


    :param cache:
    :param genome_locations:
    :param genome_locations_hg19:
    :param genome_version_q:
    :return:
    """
    if cache is None:
        mode =0
    else:
        mode = cache.mode

    if mode == 0:
        # Online mode
        variant_data = {}
        for var in genome_locations:
            try:
                res = parse_genome_position(var)
                chr, ref_seq, pos, ref, alt = res[0], res[1], res[2], res[3], res[4]

                res = api_requests(chr, pos, ref, alt, genome_version_q)
                variant_data[var] = {}
                variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = res
            except:
                variant_data[var] = {}
                print("error in request for: ",var)
                print(traceback.format_exc())
    elif mode == 1:
        variant_data = {}
        for var in genome_locations:
            res = parse_genome_position(var)
            if res is not None:
                chr, ref_seq, pos, ref, alt = res[0], res[1], res[2], res[3], res[4]

                # Check if biomarker is in cache
                response = cache.get_genompos(var)
                #response = {}

                if response is not None:
                    variant_data[var] = {}
                    variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = response
                else:
                    # query API
                    res = api_requests(chr, pos, ref, alt, genome_version_q)
                    variant_data[var] = {}
                    variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = res

                    # Store variant in API cache
                    cache.add_genompos(var, res)
    else:
        # Offline mode - Return variant data from the API cache only
        variant_data = {}
        for var in genome_locations:
            response = cache.get_genompos(var)
            if response is not None:
                variant_data[var] = {}
                variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = response
            else:
                variant_data[var] = {}
                variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = {}

    if genome_locations_hg19 is not None:
        variant_data = get_hg19_positions_as_keys(variant_data, genome_locations, genome_locations_hg19)

    # extract relevant features in key-value section
    #variant_data = extract_oncokb_data(variant_data)

    # normalize features
    if (genes is not None) and (aa_exchanges is not None):
        variant_data = normalize_oncokb_feature_keys(variant_data, genome_locations, genes, aa_exchanges)
    #variant_data = split_multiple_pmids_in_separate_evidence_results(variant_data)

    # map evidence levels
    variant_data = map_evidence_levels(variant_data)

    return variant_data


def api_requests(chr, pos, ref, alt, genome_version_q):
    """

    :param chr:
    :param pos:
    :param ref:
    :param alt:
    :param genome_version_q:
    :return:
    """

    res = {}
    for match_type in conf_reader.match_types:
        res[match_type] = []

    # exact match
    res["exact_match"] = api_request_by_genomic_change(chr, pos,ref,alt, genome_version_q)

    return res


def api_request_by_genomic_change(chr, pos, ref, alt, genome_version_q):
    """
    Searches clinical significance data from the OncoKB API by searching for a genomic location

    :param chr:
    :param pos:
    :param ref:
    :param alt:
    :param genome_version_q:
    :return:
    """
    url = conf_reader.config["DEFAULT"][
              "API_URL_SEARCH_BY_GENOMIC_CHANGE"] + "=" + chr + "," + pos + "," + pos + "," + ref + "," + alt
    if genome_version_q != '':
        url += '&referenceGenome=' + genome_version_q
    print(url)
    res = requests.get(url,
                       headers={"Authorization": "Bearer " + conf_reader.__ONCOKB_KEY__})
    try:
        res = res.json()
    except:
        print(traceback.format_exc())
        res = {}

    return res

def generate_missing_key_reponse():
    res = {}

    #res

    return res