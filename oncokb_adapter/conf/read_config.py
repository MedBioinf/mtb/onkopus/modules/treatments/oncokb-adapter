import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

__ONCOKB_KEY__ = ""
if "ONCOKB_KEY" in os.environ:
    __ONCOKB_KEY__ = os.getenv("ONCOKB_KEY")
    print("loaded key ",__ONCOKB_KEY__)
else:
    try:
        key_file = config['DEFAULT']['KEY_FILE']
        file = open(key_file, 'r')
        for line in file:
            __ONCOKB_KEY__ = line
        file.close()
        print(__ONCOKB_KEY__)
    except:
        print("no key file found")

if "MODE" in os.environ:
    config["DEFAULT"]["MODE"] = os.getenv("MOED")

if "CACHE_DIR" in os.environ:
    config["CACHE"]["CACHE_DIR"] = os.getenv("CACHE_DIR")

if "LOG_FILE" in os.environ:
    config["CACHE"]["LOG_FILE"] = os.getenv("LOG_FILE")

if "MODULES_SERVER" in os.environ:
    config["MODULES"]["SERVER"] = os.getenv("MODULES_SERVER")

if "MODULES_PROTOCOL" in os.environ:
    config["MODULES"]["PROTOCOL"] = os.getenv("MODULES_PROTOCOL")

if "MODULES_PORT" in os.environ:
    config["MODULES"]["PORT"] = os.getenv("MODULES_PORT")

match_types = ["exact_match", "any_mutation_in_gene", "same_position", "same_position_any_mutation"]

def get_config(key):
    if key in config:
        return config["key"]
