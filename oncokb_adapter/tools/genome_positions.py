import re, requests
from oncokb_adapter.conf import read_config as conf_reader


def split_association(association):
    variant_exchange_long_pt_ext = '([a-z|A-Z]+)([0-9]+)([a-z|A-Z]?)'
    if re.compile(variant_exchange_long_pt_ext ).match(association):
        return re.compile(variant_exchange_long_pt_ext).match(association).groups()
    else:
        return None

def recognize_match_type(association, gene_name, aa_exchange):

    groups = split_association(association)
    groups_ref = split_association(aa_exchange)

    if groups is not None:
        if groups[2] == "":
            return "same_position_any_mutation"
        elif (groups[0] == groups_ref[0]) and (groups[1] == groups_ref[1]) and (groups[2] == groups_ref[2]):
            return "exact_match"

    return "same_position"


def parse_genome_position(ref):
    rsregex = "(NC_[0]+)([1-9|X|Y][0-9|X|Y]?).([0-9]+):(g.|c.)?([0-9]+)([A|C|G|T]+)>([A|C|G|T]+)"
    if re.compile(rsregex).match(ref):
        p = re.compile(rsregex).match(ref).groups()
        chr = p[1]
        pos = p[4]
        ref_seq = p[3]
        ref = p[5]
        alt = p[6]
        return chr, ref_seq, pos, ref, alt
    else:
        rsregex = "(CHR|chr)([0-9|X|Y]+):(g.|c.)?([0-9]+)([A|C|G|T]+)>([A|C|G|T]+)"
        if re.compile(rsregex).match(ref):
            p = re.compile(rsregex).match(ref).groups()
            chr = p[1]
            pos = p[3]
            ref_seq = p[2]
            ref = p[4]
            alt = p[5]
            return chr, ref_seq, pos, ref, alt
    return None

def get_connection(url):
    """
    Requests a module over a HTTP GET request

    :param url:
    :return:
    """
    print(url)
    r = requests.get(url)
    return r.json()

def get_liftover(genome_locations):
    liftover_q = ''
    variants = {}
    for genompos in genome_locations:
        res = parse_genome_position(genompos)
        if res is not None:
            chrom, ref_seq, pos, ref, alt = res[0], res[1], res[2], res[3], res[4]
            liftover_q += 'chr'+chrom + ":" + pos +","

            chr_pos = 'chr'+chrom + ":" + pos
            variants[chr_pos] = {}
            variants[chr_pos]["chrom"] = 'chr'+chrom
            variants[chr_pos]["pos_hg19"] = pos
            variants[chr_pos]["ref"] = ref
            variants[chr_pos]["alt"] = alt
    liftover_q = liftover_q.rstrip(",")

    liftover_url = conf_reader.config["MODULES"]["PROTOCOL"] + '://' + conf_reader.config["MODULES"]["SERVER"] + \
                   conf_reader.config["MODULES"]["PORT"] + '/CCS/v1/liftover/hg19:hg38/' + liftover_q
    response = get_connection(liftover_url)

    for result in response:
        variants[result["header"]["qid"]]["pos_hg38"] = str(result["data"]['position'])

    genome_locations_hg38 = []
    for key in variants.keys():
        print(variants[key])
        new_pos = variants[key]["chrom"] + ':' + variants[key]["pos_hg38"] + \
                                      variants[key]["ref"] + ">" + variants[key]["alt"]
        genome_locations_hg38.append( new_pos )

    return genome_locations_hg38

def get_hg19_positions_as_keys(biomarker_data, genome_locations_hg38, genome_locations_hg19):

        biomarker_data_hg19 = {}
        for i,var in enumerate(genome_locations_hg38):
            biomarker_data_hg19[genome_locations_hg19[i]] = biomarker_data[genome_locations_hg38[i]]
            #biomarker_data_hg19[genome_locations_hg19[i]]['variant_data']['pos_hg38'] = genome_locations_hg38[i]

        return biomarker_data_hg19