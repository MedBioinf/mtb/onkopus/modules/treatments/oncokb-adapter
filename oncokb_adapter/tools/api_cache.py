import json, traceback
from os import walk
import datetime
import os
from os import path
from oncokb_adapter.conf import read_config as conf_reader
from oncokb_adapter.api import api_request_by_genomic_change
from oncokb_adapter.tools.genome_positions import parse_genome_position


class APICache:

    def __init__(self):
        self.filepath = conf_reader.config["CACHE"]["CACHE_DIR"]
        self._setup_cache()

        try:
            logfile = open(conf_reader.config["CACHE"]["LOG_FILE"],'r')
            self.log = json.load(logfile)
            logfile.close()
        except:
            print("error loading cache log")
            print(traceback.format_exc())
            self._set_default_log()

        if conf_reader.config["DEFAULT"]["MODE"] == 0:
            self.mode = 0
        else:
            self.mode = 1
        self.cache = self._load_cache()

    def _set_default_log(self):
        self.log = {"last_updated": str(datetime.datetime.today().strftime('%Y-%m-%d')), "genome_version": 'GRCh38'}

    def _setup_cache(self):
        if not path.exists(conf_reader.config["CACHE"]["CACHE_DIR"]):
            os.makedirs(conf_reader.config["CACHE"]["CACHE_DIR"])
        if not path.exists(conf_reader.config["CACHE"]["LOG_FILE"]):
            self._set_default_log()

            outfile = open(conf_reader.config["CACHE"]["LOG_FILE"], 'w')
            json.dump(self.log, outfile)
            outfile.close()

    def _update_cache(self):
        """
        Update cache for all known variants. Queries the OncoKB API for all known variants and replaces the files with the new data

        :return:
        """
        #self.log["last_update_start"] = datetime.datetime().now().strftime('%Y%m%d %H:%M:%s')
        filenames = next(walk(self.filepath), (None, None, []))[2]
        for filename in filenames:
            genompos = filename.replace('_', ':').replace('-', '>')[:-5]

            # Request to OncoKB API
            res = parse_genome_position(genompos)
            chrom, ref_seq, pos, ref, alt = res[0], res[1], res[2], res[3], res[4]
            response = api_request_by_genomic_change(chrom, pos, ref, alt, self.log["genome_version"])

            # Save response in cache file
            genompos_filename = genompos.replace(':','_').replace('>','-') + '.json'
            outfile = open(self.filepath + genompos_filename, 'w')
            json.dump(response, outfile)
            outfile.close()

        # Update log file
        #self.log["last_update_finish"] = datetime.datetime().now().strftime('%Y%m%d %H:%M:%s')
        self.log["last_updated"] = datetime.datetime.today().strftime('%Y-%m-%d')
        logfile = open(conf_reader.config["CACHE"]["LOG_FILE"], 'w')
        json.dump(self.log, logfile)
        logfile.close()

    def check_for_update(self):
        time_delta = datetime.datetime.today() - datetime.datetime.strptime(self.log["last_updated"], '%Y-%m-%d')
        #print(time_delta.days)
        if int(time_delta.days) > int(conf_reader.config["CACHE"]["DELTA_UPDATE"]):
            self._update_cache()

    def _load_cache(self):
        cache = {}
        filenames = next(walk(self.filepath), (None, None, []))[2]
        for filename in filenames:
            genompos = filename.replace('_',':').replace('-','>')[:-5]
            cache[genompos] = filename

        return cache

    def add_genompos(self, var, response):
        if var not in self.cache.keys():
            filename = str(var.replace(':','_').replace('>','-') + '.json')

            try:
                outfile = open(self.filepath + '/' + filename, 'w')
                json.dump(response, outfile)
                outfile.close()
            except:
                print(traceback.format_exc())

            self.cache[var] = filename

    def get_genompos(self, var):
        if var in self.cache.keys():
            try:
                filename = conf_reader.config["CACHE"]["CACHE_DIR"] + str(var.replace(':','_').replace('>','-') + '.json')
                file = open(filename,'r')
                var_data = json.load(file)
                file.close()
                return var_data
            except:
                return None
        else:
            return None
