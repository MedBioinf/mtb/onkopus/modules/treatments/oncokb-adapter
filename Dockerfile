FROM python:3.12.0rc1-bookworm

RUN apt-get update
RUN apt-get upgrade -y

RUN python3 -m pip install --upgrade pip

RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip

WORKDIR /app/oncokb-adapter
COPY ./requirements.txt /app/oncokb-adapter/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/oncokb-adapter/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 10112

CMD [ "/opt/venv/bin/python3", "/app/oncokb-adapter/app.py" ]
