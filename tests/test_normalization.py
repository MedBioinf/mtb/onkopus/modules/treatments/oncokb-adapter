import unittest
import oncokb_adapter
import oncokb_adapter.api.request
from oncokb_adapter.tools.genome_positions import parse_genome_position, get_hg19_positions_as_keys
import oncokb_adapter.conf.read_config as conf_reader

class TestNormalization(unittest.TestCase):

    def test_evidence_level_mapping(self):
        genompos = 'chr12:g.25245350C>T'
        genome_locations = genompos.split(",")
        genome_version="hg38"



        genome_version_q = ''
        genome_locations_hg19 = None
        if (genome_version == 'hg38') or (genome_version == 'GRCh38'):
            genome_version_q = 'GRCh38'

        var = genompos
        chr, ref_seq, pos, ref, alt = parse_genome_position(var)
        res = oncokb_adapter.api.api_request_by_genomic_change(chr, pos, ref, alt, genome_version_q)
        variant_data = {}
        variant_data[var] = {}
        variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = res

        variant_data = oncokb_adapter.api.request.extract_oncokb_data(variant_data)

        # normalize features
        variant_data = oncokb_adapter.api.request.normalize_oncokb_feature_keys(variant_data)

        # map evidence levels
        variant_data = oncokb_adapter.api.request.map_evidence_levels(variant_data)

        print(variant_data)
