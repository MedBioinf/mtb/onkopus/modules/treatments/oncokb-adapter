import unittest
from app import app

class TestFlaskServices(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_cache_update(self):
        genompos = 'chr12:g.25245350C>T'
        response = self.client.get("/oncokb/v1/hg38/update_cache")
        print(response.get_data(as_text=True))

    def test_liftover(self):
        genompos = "chr7:21784210A>AG,chr10:8115913C>T,chr17:7681744T>C,chr1:144886115G>A,chr1:237801664G>A,chr5:13883075C>T,chr7:91659250A>G,chr11:62287818C>T,chr11:62292053G>T,chr12:104099498C>A,chr16:89825019T>A,chr17:58988043G>A"
        genome_version ='hg19'
        response = self.client.get("/oncokb/v1/ " +genome_version +"/full?genompos= " +genompos)
        print(response.get_data(as_text=True))