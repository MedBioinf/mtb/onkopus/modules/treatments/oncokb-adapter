import unittest, json
import oncokb_adapter.tools.api_cache


class TestOncoKBCache(unittest.TestCase):

    def test_cache_loading(self):
        cache = oncokb_adapter.tools.api_cache.APICache()

        file = open('./test_files/test.json','r')
        response = json.load(file)
        file.close()

        var = 'chr1:114713908T>A'
        cache.add_genompos(var, response)

    def test_update(self):
        cache = oncokb_adapter.tools.api_cache.APICache()
        cache.check_for_update()