import unittest
import oncokb_adapter.tools.genome_positions

class TestParsingAssociation(unittest.TestCase):

    def test_parse_association(self):
        association = "V600"
        ref = "V600E"

        mt = oncokb_adapter.tools.genome_positions.recognize_match_type(association,None,ref)
        print("Test any mutation same pos")
        self.assertEqual(mt,"same_position_any_mutation","")

    def test_parse_association1(self):
        association = "V600E"
        ref = "V600E"

        mt = oncokb_adapter.tools.genome_positions.recognize_match_type(association,None,ref)
        print("Test exact match")
        self.assertEqual(mt,"exact_match","")

    def test_parse_association2(self):
        association = "V600K"
        ref = "V600E"

        mt = oncokb_adapter.tools.genome_positions.recognize_match_type(association,None,ref)
        print("Test same position")
        self.assertEqual(mt,"same_position","")
