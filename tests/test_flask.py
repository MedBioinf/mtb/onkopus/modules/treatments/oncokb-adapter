import unittest, json
from app import app

class TestFlaskServices(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_variant_request(self):
        #genompos='chr7:140453136A>T,chr10:8115913C>T'
        genompos='chr12:g.25245350C>T'
        genompos="chr7:140753336A>T"
        aa_exchange = "V600E"
        genes = "BRAF"
        response = self.client.get("/oncokb/v1/hg38/full?genompos=" + genompos+ "&genes=" + genes + "&aa_exchange=" + aa_exchange)
        data = json.loads(response.get_data(as_text=True))
        print(data["chr7:140753336A>T"])
        #print(data[genompos])

    def test_variant_request_simple(self):
        #genompos='chr7:140453136A>T,chr10:8115913C>T'
        genompos='chr12:g.25245350C>T'
        genompos="chr7:140753336A>T"
        aa_exchange = "V600E"
        genes = "BRAF"
        response = self.client.get("/oncokb/v1/hg38/full?genompos=" + genompos)
        data = json.loads(response.get_data(as_text=True))
        print(data["chr7:140753336A>T"])
        #print(data[genompos])
