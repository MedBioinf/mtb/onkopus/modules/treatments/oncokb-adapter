import unittest
import oncokb_adapter.api.request
import oncokb_adapter.conf.read_config as conf_reader

class FeatureKeyExtractionTest(unittest.TestCase):

    def test_key_extraction(self):
        var="chr7:140753336A>T"
        chrom="7"
        pos="140753336"
        ref="A"
        alt="T"
        genome_version_q="GRCh38"
        variant_data={}
        variant_data[var] = {}
        variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]] = oncokb_adapter.api.request.api_request_by_genomic_change(chrom, pos, ref, alt, genome_version_q)
        print(variant_data[var][conf_reader.config["MODULES"]["ONCOKB_PREFIX"]])

        variant_data = oncokb_adapter.api.request.normalize_oncokb_feature_keys(variant_data)
        print(variant_data)

