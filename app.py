import copy
from flask import Flask, request
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
import oncokb_adapter.tools.api_cache
from oncokb_adapter.tools.genome_positions import get_liftover
from oncokb_adapter.api import api_request_by_genomic_change
import oncokb_adapter
import oncokb_adapter.conf.read_config as conf_reader

app = Flask(__name__)
app.config.from_object(__name__)

SERVICE_NAME='oncokb'
VERSION='v1'
CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = '/' + SERVICE_NAME + '/v1/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "OncoKB Adapter"
    },
)

cache = oncokb_adapter.tools.api_cache.APICache()
if conf_reader.config["DEFAULT"]["MODE"] == 0:
    active_cache = None
else:
    active_cache = cache


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/update_cache', methods=['GET'])
def update_cache(genome_version=None):
    cache.check_for_update()
    return ''


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/full', methods=['GET'])
def get_interpretations(genome_version=None):
    genompos = request.args.get("genompos")

    genes = request.args.get("genes")
    if genes is not None:
        genes = genes.split(",")
    aa_exchanges = request.args.get("aa_exchange")
    if aa_exchanges is not None:
        aa_exchanges = aa_exchanges.split(",")

    key_found = False

    # OncoKB key
    if 'oncokb-key' in request.headers:
        key = request.headers['oncokb-key']
        print("Received key: ",key)
        conf_reader.__ONCOKB_KEY__ = key
        key_found = True
    elif conf_reader.__ONCOKB_KEY__ != "":
        key_found = True

    if key_found is False:
        print("No key passed in HTTP header")
        return oncokb_adapter.api.generate_missing_key_reponse()

    genome_locations = genompos.split(",")

    genome_version_q = ''
    genome_locations_hg19 = None
    if (genome_version == 'hg38') or (genome_version == 'GRCh38'):
        genome_version_q = 'GRCh38'
    elif (genome_version == 'hg19') or (genome_version == 'GRCh37'):
        genome_locations_hg19 = copy.deepcopy(genome_locations)
        genome_locations = get_liftover(genome_locations)
        genome_version_q = 'GRCh38'

    variant_data = oncokb_adapter.api.process_request(cache=active_cache,
                                                      genome_locations=genome_locations,
                                                      genome_locations_hg19=genome_locations_hg19,
                                                      genome_version_q=genome_version_q,
                                                      genes = genes,
                                                      aa_exchanges = aa_exchanges
                                                      )
    return variant_data


if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=10112)

